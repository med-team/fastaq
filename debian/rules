#!/usr/bin/make -f

export DH_VERBOSE := 1
export PYBUILD_NAME=fastaq
export PYBUILD_TEST_PYTEST = 1

mandir := $(CURDIR)/debian/man
debfolder := $(CURDIR)/debian

%:
	dh $@ --buildsystem=pybuild

override_dh_auto_build:
	dh_python3
	dh_auto_build
	mkdir $(CURDIR)/doc
	cd $(CURDIR)/doc

override_dh_auto_clean:
	rm -rf build .pybuild doc pyfastaq.egg-info .pytest_cache
	find . -name __pycache__ | xargs rm -rf
	rm -rf $(mandir)

override_dh_installman:
	mkdir -p $(mandir)
	PYTHONPATH=. help2man -N -o debian/man/fastaq.1 \
	  -n 'FASTA and FASTQ file manipulation tools' \
	  --no-discard-stderr \
	  --version-string=`PYTHONPATH=. scripts/fastaq version` scripts/fastaq
	sed -i 's/.SH DESCRIPTION/.SH DESCRIPTION\\n.nf/' debian/man/fastaq.1
	scripts/fastaq 2>&1 | tail -n +13 | debian/make_man
	dh_installman --

override_dh_auto_test:
	PYBUILD_SYSTEM=custom \
	PYBUILD_TEST_ARGS="cd {dir}; {interpreter} -m pytest pyfastaq/tests/" dh_auto_test
